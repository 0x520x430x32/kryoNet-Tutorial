package server;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.Server;
import packages.request.PingRequest;
import packages.response.PingResponse;

import java.io.IOException;

public class Main {

    public static void main(String[] args) {

        Server server = new Server();

        server.start();

        try {
            server.bind(25444);
        } catch (IOException e) {
            e.printStackTrace();
        }

        server.addListener(new ServerListener());

        Kryo kryo = server.getKryo();
        kryo.register(PingRequest.class);
        kryo.register(PingResponse.class);

    }

}
